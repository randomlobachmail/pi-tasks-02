#include <stdio.h>
#include <time.h>

int main(){
	int length, i, j;
	char pass[256];
	
	printf("Enter the length of password: \n");
	scanf("%d",&length);
	srand(time(0));

	if (length<3){
		puts("Incorrect password!\n");
		return 1;
	}
				
	for (j = 0; j < 40; ++j){

		pass[0] = rand() % 10 + '0'; //������� ��������� ���� � ������� � ������ ��������
		pass[1] = rand() % 26 + 'A'; // � ���� �������������� 
		pass[2] = rand() % 26 + 'a'; 

		for (i = 3; i < length; ++i){
			switch(rand() % 3){
				case 0:
					pass[i] = rand() % 10 + '0';
					break;
				case 1:
					pass[i] = rand() % 26 + 'A';
					break;
				case 2:
					pass[i] = rand() % 26 + 'a';
					break;
			}
		}		
		for (i = 0; i < length; ++i)
			printf("%c", pass[i]);
		if (j % 2 == 0)
			printf(" ");
		else
			printf("\n");		
	}
	return 0;
}