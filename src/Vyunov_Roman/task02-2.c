#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <conio.h>

#define BUF_LENGTH 256
typedef unsigned int UInt;

int main()
{
	UInt real_length = 0, length = 0, TrueTry=0;
	char pass_buf[]="ITMM";
	real_length = strlen(pass_buf);
	printf("Enter password:");
	while ((length<real_length)&&(TrueTry<real_length))
	{
		if (_getch()==pass_buf[length])
		{
			TrueTry++;
		}
		length++;
		putchar('*');
	}
	printf("\n");
	if (TrueTry==real_length)
	{
		printf("True password!\n");
	}
	else
	{
		printf("Wrong password!\n");
	}
	return 0;
}