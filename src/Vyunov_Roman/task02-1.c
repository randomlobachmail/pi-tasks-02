#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define BUF_LENGTH 256

int second_passFLG = 0;

char random_char(void)
{
	switch (rand() % 3)
	{
	case 0:
		return (rand() % ('9' - '0' + 1) + '0');
		break;
	case 1:
		return (rand() % ('Z' - 'A' + 1) + 'A');
		break;
	case 2:
		return (rand() % ('z' - 'a' + 1) + 'a');
		break;
	}
}

void pass_create(char*arr, unsigned int length)
{
	unsigned int i = 0;
	unsigned int cap_letters = 0, lowercase = 0, number = 0;

	while (!cap_letters || !lowercase || !number)
	{
		cap_letters = 0; lowercase = 0; number = 0;
		for (i = 0; i < length; i++)
		{
			arr[i] = random_char();
			if (arr[i] >= 'A'&&arr[i] <= 'Z')
				cap_letters++;
			if (arr[i] >= 'a'&&arr[i] <= 'z')
				lowercase++;
			if (arr[i] >= '0'&&arr[i] <= '9')
				number++;
		}
	}

	arr[i] = '\0';
}

void pass_print(char*arr, unsigned int length,unsigned int HowMatch)
{
	unsigned int i;
	for(i=0;i<HowMatch;i++)
	{
		pass_create(arr, length);
		if (second_passFLG)
		{
			printf("%s\n", arr); second_passFLG = 0;
		}
		else
		{
			printf("%s\t", arr); second_passFLG = 1;
		}
	}
	printf("\n");
}

int main()
{
	srand(time(0));
	unsigned int length = 0;
	char pass_buf[BUF_LENGTH];
	
	printf("Enter password length: ");
	scanf("%d", &length);
	
	pass_print(pass_buf, length, 40);
	
	return 0;
}