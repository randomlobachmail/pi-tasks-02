#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
int main()
{
	int i, j, k;
	printf("Enter the interval, example 1-9:\n");
	scanf("%d-%d", &i, &j);
	printf("[");
	for (k = i; k<j; k++)
		printf("%d, ", k);
	printf("%d] ", j);
}