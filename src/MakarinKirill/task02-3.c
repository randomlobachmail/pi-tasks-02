//Не сделал 2 задачу, так как не нашел под osX библиотеки <conio.h>

#include <stdio.h>

int main()
{
    int start = 0, end = 0; // Соответственно - начало интервала, конец интервала
    int i;
    
    printf("Enter a range start and range end. As like that 5-10 \n");
    scanf("%d-%d", &start, &end);
    
    while ((start <= 0) || (end <= 0) || (end<start))
    {
        printf("Wrong input data, please repeat\n");
        scanf("%d-%d", &start, &end);
    }
    
    for (i = start; i <= end; ++i)
        printf("%d ", i);
    
    printf("\n");
    
    return 0;
}
