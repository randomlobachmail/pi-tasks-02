#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int randomChar(int *flag)
{
    int val = rand()%3;
    
    while (*flag == val)
        val = rand()%3;
    
    switch(val)
    {
        case 0:
            *flag = val;
            return (rand()%(57-48+1)+48); // генерация случайной цифры
        case 1:
            *flag = val;
            return (rand()%(122-97+1)+97); // генерация случайной буквы а..z
        case 2:
            *flag = val;
            return (rand()%(90-65+1)+65);  // генерация случайной буквы А..Z
    }
}

int main()
{
    srand(time(0));

    int lengthPass, i, j, k, flag = -1; // flag- исползуется для того, чтобы понять, какая из 3х групп символов была использована, для выбора предыдущего символа.

    printf("Enter a pass lenght \n");
    scanf("%d", &lengthPass);

    for (i = 0; i < 20; ++i)
    {
        //генерация 1го пароля в столбце
        for (j = 0; j < lengthPass; ++j)
            putchar( randomChar(&flag) );

        printf("    ");
 
        //генерация 2го пароля в столбце
        for (k = 0; k < lengthPass; ++k)
            putchar( randomChar(&flag) );

        printf(" \n");
    }

    return 0;
}

