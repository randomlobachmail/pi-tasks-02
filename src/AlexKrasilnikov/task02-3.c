#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <string.h>

void main()
{
    int fn, sn;
    printf("Enter two numbers in next format a - b\n");
    scanf("%d - %d", &fn, &sn);
    while (sn <= fn)
    {
        printf("Error, try again\n");
        scanf("%d - %d", &fn, &sn);
    }
    for (int i = fn; i <= sn; i++) 
        if (i <= 0) printf("");
        else printf("%d ", i);
}