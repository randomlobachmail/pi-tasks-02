#define _CRT_SECURE_NO_WARNINGS
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
void gen(int n)
{
	int i = 0, t=0;
	char letsmall[] = { 'a', 'b', 'c', 'd', 'i', 'f', 'g', 'h', 'i', 'j' },
		letbig[] = { 'A', 'B', 'C', 'D', 'I', 'F', 'G', 'H', 'I', 'J' },
		number[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' },
		fin[256];
	srand(time(0));
	for (int j = 0; j < 40; j++)
	{
		for (i = 0; i <= n - 1; i++)
		{

			t = rand() % 3;
			if (t == 0)
			{
				fin[i] = rand() % ('z' - 'a' + 1) + 'a';
			}
			if (t == 1)
			{
				fin[i] = rand() % ('Z' - 'A' + 1) + 'A';
			}
			if (t == 2)
			{
				fin[i] = rand() % ('9' - '0' + 1) + '0';
			}
		}
		for (i = 0; i < n; i++)
		{
			printf("%c", fin[i]);
		}
			printf("\n");
		
	}
}
int main()
{
	int n = 0;
	scanf("%d", &n);
	gen(n);
	return 0;
}