#include <stdio.h>

int main()
{
	int i,start=-1,finish=-1;
	printf("Enter your line segment in format *-* >> ");
	scanf("%d-%d", &start, &finish);
	printf("\"%d-%d\" >> ",start,finish);
	if (start > finish) 
	{
		puts("range error: finish < start");
		return 1;
	}
	if (start<=0 || finish <= 0) 
	{
		puts("Error data entry");
		return 2;
	}
	for (i=start;i<finish;i++)
		printf("%d,",i);
	printf("%d \n",finish);
	return 0;
	}
