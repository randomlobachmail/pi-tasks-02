#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main()
{
	int firstNumber = 0, lastNumber = 0, i = 0;
	printf("Enter the range of numbers (xxx-xxx) ");
	scanf("%d-%d", &firstNumber, &lastNumber);
	if (firstNumber <= '0' || lastNumber <= '0' || firstNumber > lastNumber)  // �������� �� ���������� ����������� ��������
	{
		printf("Input error! Try again \n");
		return 1;
	}
	if (firstNumber == lastNumber)
	{
		printf("[%d] \n", firstNumber);
		return 0;
	}
	printf("[ ");
	for (i = firstNumber; i <= lastNumber; i++)
	{
		printf("%d ", i);
	}
	printf("]\n");
	return 0;
}