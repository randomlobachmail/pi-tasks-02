#define _CRT_SECURE_NO_WARNINGS
#define N 40
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void generatePassword(char *password, int size) // ������� ��������� ������ �� ��������� ��������
{
	int i;
	for (i = 0; i < (size+1); ++i)
	{
		switch (rand() % 3)
		{
		case 0:
			password[i] = rand() % 10 + '0'; //��������� � ������ ��������� �����
			break;
		case 1: 
			password[i] = rand() % 26 + 'A'; //��������� ��������� ��������� �����
			break;
		case 2:
			password[i] = rand() % 26 + 'a'; //��������� ��������� �������� �����
		}
		password[size] = 0;
		putchar(password[i]);
	}
}

int main()
{
	int i, j;
	int l; // ����� ������
	char password[N];
	printf("Enter the lengh of one password: ");
	scanf("%d", &l);
	if (l < 5 && l > 0) // �������� �� ������������ �������� ������
	{
		printf("The password will be unsafe \nUse 10 or more symbols to protect your data\n");
		return 1;
	}
	else if (l <= 0) // �������� �� ��������� ����������� ��������
	{
		printf("Input error! Try again\n");
		return 1;
	}
	srand(time(0));
    for (j = 0; j < N; j++)
	{
		putchar((j+1) % 2 == 0 ? ' ' : '\n'); // ����� ������� � 2 �������
		generatePassword(password, l);
	}
	printf("\n");
	return 0;
}