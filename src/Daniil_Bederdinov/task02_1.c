#define _CRT_SECURE_NO_WARNINGS
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main()
{
    int len = 0;
    int table = 1;
    char alph[] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'v', 'x', 'y', 'z',
                    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'V', 'X', 'Y', 'Z',
                    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
    char str[256] = {0};
    srand(time(NULL));
    printf("Enter password length: ");
    scanf("%i",&len);
    if (len < 1)
    {
        printf("Wrong value \n");
        return 1;
    }
    else
    {
        for (int k = 0; k < 40; k++)
        {
            for (int i = 0; i < len; i++)
            {
                str[i] = alph[rand() % 54];
            }

            for (int j = 0; j < strlen(str); j++)
                printf("%c", str[j]);

            if (table == 1)
            {
                table++;
                printf("    ");
            }
            else if (table == 2)
            {
                puts("");
                table = 1;
            }
        }
        return 0;
    }
}