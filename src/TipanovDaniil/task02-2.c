#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <conio.h>
int main()
{
	char originPass[10] = {'q', 'w', 'e', 'r', 't', 'y', '1', '2', '3', '4'};
	char pass[256];
	int i = 0;
	int flag = 1;
	printf("Enter the pass: ");
	while (flag)
	{
		pass[i] = _getch();
		if (pass[i] == '\b')
			printf("\b \b");
		else if (pass[i] == '\r')
		{
			flag = 0;
			printf("\n");
		}
		else
			printf("*");
		i++;
	}
	i = 0;
	while (pass[i] != '\r')
	{
		if (pass[i] != originPass[i])
		{
			printf("Incorrect password!\n");
			return 1;
		}
		else
			i++;
	}
	if (i == 10)
		printf("Success!\n");
	else
		printf("Incorrect password!\n");
	return 0;
}