#include <stdio.h>
#include <conio.h>
int main()
{
	char s[256];
	fgets(s, 256, stdin);
	int i = 0, n1 = 0, n2 = 0;
	while (s[i] != '-') {
		n1 = n1 * 10 + s[i] - '0';
		++i;
	}
	for (i = i + 1; i < strlen(s) - 1; ++i)
		n2 = n2 * 10 + s[i] - '0';
	printf("[");
	for (i = n1; i < n2; ++i)
		printf("%i,", i);
	printf("%i]", n2);
	return 0;
}